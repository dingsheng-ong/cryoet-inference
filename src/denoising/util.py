from typing import Tuple

import numpy as np
# import onnxruntime as rt
import torch
from torch.nn import functional as F

from torch.nn import functional as F
from tqdm import tqdm


def normalise(*arr: Tuple[np.ndarray], eps: float = 1e-9) -> Tuple[np.ndarray]:
    out = []
    t = np.stack(arr)
    u, v = t.min(), np.clip(t.max() - t.min(), a_min=eps, a_max=None)
    for x in arr:
        out.append((x - u) / v)
    return out


def standardise(*arr: Tuple[np.ndarray], eps: float = 1e-9) -> Tuple[np.ndarray]:
    out = []
    t = np.stack(arr)
    mean, std = t.mean(), np.clip(t.std(), a_min=eps, a_max=None)
    for x in arr:
        out.append((x - mean) / std)
    return out


def tiled_inference(
    model: torch.nn.Module,
    data: torch.Tensor,
    patch_size: int = 64,
    overlap_size: int = None,
    batch_size: int = 1,
    use_gpu: bool = False,
) -> torch.Tensor:
    ot = overlap_size
    _, _, *size = data.shape

    pad_sizes = []
    for x in size:
        n = int(np.ceil(x / (patch_size - 2 * ot)))
        y = (patch_size - 2 * ot) * n
        p = y - x
        pad_sizes = pad_sizes + [p // 2, p - p // 2]

    data = F.pad(data, pad_sizes[::-1], mode="replicate")
    padded = F.pad(data, (ot,) * 6, mode="replicate")
    patches = padded
    for i in range(3):
        assert (
            patch_size - 2 * ot > 0
        ), f"patch size must be greater than {2 * ot} ({patch_size})"
        patches = patches.unfold(i + 2, patch_size, patch_size - 2 * ot)

    n_patches_per_dim = patches.shape[2:5]
    patches = patches.contiguous().view(1, -1, *(patch_size,) * 3).transpose(0, 1)
    output_patches = []
    nb_patches = int(np.ceil(patches.size(0) / batch_size))

    for x in tqdm(patches.split(batch_size, dim=0), total=nb_patches):
        if torch.cuda.is_available() and use_gpu:
            x = x.cuda()
        with torch.no_grad():
            y = model(x)[..., *(slice(ot, -ot),) * 3]
        output_patches.append(y.detach().cpu())
    output_patches = torch.cat(output_patches, dim=0)
    output = output_patches.transpose(0, 1).view(
        1, 1, *n_patches_per_dim, *output_patches.shape[-3:]
    )
    output = output.contiguous().permute(0, 1, 2, 5, 3, 6, 4, 7)
    output = output.reshape(data.shape)

    return output[
        ...,
        *(
            slice(b, s if a == 0 else -a)
            for s, a, b in zip(size, pad_sizes[::2], pad_sizes[1::2])
        ),
    ]
