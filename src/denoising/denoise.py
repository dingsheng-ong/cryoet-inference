import click
import mrcfile
import numpy as np
import torch

from .util import standardise, tiled_inference


@click.command("denoise")
@click.argument("input", nargs=-1, type=click.Path(exists=True))
@click.option(
    "-o", "--output", default="./denoised.mrc", type=click.Path(dir_okay=True)
)
@click.option("-m", "--model", required=True, type=click.Path(exists=True))
@click.option("--use-gpu", is_flag=True)
@click.option("--overlap-tile", is_flag=True)
@click.option("-ps", "--patch-size", default=64, help="patch size for --overlap-tile")
@click.option(
    "-os", "--overlap-size", default=8, help="overlap size for --overlap-tile"
)
@click.option("-bs", "--batch-size", default=1, help="batch size for --overlap-tile")
def cli(
    input, output, model, use_gpu, overlap_tile, patch_size, overlap_size, batch_size
):
    assert len(input) > 0, "no input tomogram"
    assert len(input) < 3, f"expect <= 2 maps, (got: {len(input)})"

    model = torch.jit.load(model)
    model.eval()

    denoised = 0
    input_arr = [np.array(mrcfile.read(f), dtype=np.float32, copy=True) for f in input]
    input_arr = standardise(*input_arr)

    for arr in input_arr:
        arr = torch.from_numpy(arr).unsqueeze(0).unsqueeze(0)
        if overlap_tile:
            out = tiled_inference(
                model, arr, patch_size, overlap_size, batch_size, use_gpu
            )[0, 0]
        else:
            if torch.cuda.is_available() and use_gpu:
                arr = arr.cuda()
            with torch.no_grad():
                out = model(arr)[0, 0].detach().cpu()
        denoised += out

    denoised = denoised.cpu().numpy() / len(input)
    with mrcfile.new(output, overwrite=True) as mrc:
        mrc.set_data(denoised)
    print(f"output saved to: {output}")


if __name__ == "__main__":
    cli()
