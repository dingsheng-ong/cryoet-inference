# CryoET Denoising Inferencing
---
This package implements the denoising script for electron density maps. The repository for the training code can be found at [placeholder](#).

## Installtion

**Requirements**
- `Python >= 3.11`
- `PyTorch >= 2.0`

Please install `torch>=2.0` by following the instructions on the [official site](https://pytorch.org/get-started/locally/). Then, run the following command:
```bash
pip install .
```

## Directory Structure
Ensure that `model-cpu.pt` and `model-gpu.pt` are included in the root directory. Alternatively, you can export your own model using `torch.jit.save`.
```
├── LICENSE
├── model-cpu.pt
├── model-gpu.pt
├── pyproject.toml
├── README.md
├── src
│   └── denoising
│       ├── denoise.py
│       ├── __init__.py
│       └── util.py
└── tests
```

## Usage
```
Usage: denoise [OPTIONS] [INPUT]...

Options:
  -o, --output PATH
  -m, --model PATH             [required]
  --use-gpu
  --overlap-tile               using overlap-tile strategy
  -ps, --patch-size INTEGER    patch size for --overlap-tile
  -os, --overlap-size INTEGER  overlap size for --overlap-tile
  -bs, --batch-size INTEGER    batch size for --overlap-tile
  --help                       Show this message and exit.
```

**Examples**
To denoise a single density map using **CPU**:
```bash
denoise input.mrc -o denoised.mrc -m model-cpu.pt
```
To denoise half maps using **GPU**, with the overlap-tile strategy, using a patch size of **64x64x64** and overlapping **8** voxels, processing **16** patches at once:
```bash
denoise map1.mrc map2.mrc -o denoised.mrc -m model-gpu.pt --use-gpu --overlap-tile -ps 64 -os 8 -bs 16
```
